-- MySQL dump 10.19  Distrib 10.3.30-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: blog
-- ------------------------------------------------------
-- Server version	10.3.30-MariaDB-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article` (
  `articleId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `userId` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `date` datetime DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`articleId`),
  KEY `fk_userId` (`userId`),
  CONSTRAINT `fk_userId` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article`
--

/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` VALUES (5,1,'update','# Test d\'article\n\nCeci est un test en markdown\n\n## update\n\ntest image \n\n(https://gitlab.com/KevinRSI/React-Bank/-/raw/master/readme_img/Diagramme%20budget.png)\n\ntest lien\n\n![AJAX](https://gitlab.com/KevinRSI/React-Bank/-/raw/master/readme_img/axios.png)','2021-07-05 00:00:00','https://static.fnac-static.com/multimedia/Images/FD/Comete/125059/CCP_IMG_ORIGINAL/1631241.jpg'),(6,1,'fdsggfqsdg','test','2021-07-05 00:00:00',NULL),(7,1,'fdsggfqsdg','test','2021-07-05 00:00:00',NULL),(8,1,'fgfqsdg','test','2021-07-05 00:00:00',NULL),(9,1,'New Article','\n# Test new article\n\ntest test\n','2021-07-30 10:24:59','https://cdn.filestackcontent.com/IfTulGVsRnaS7N61MJny'),(10,1,'Another new articled','test testd','2021-07-30 10:48:26',NULL),(11,1,'The Crowned Clown','\n# Chapter 1 : Allen Walker\nlorem','2021-07-30 10:53:09',NULL),(16,1,'Patate Vraiment Vraiment Chaude','\n# test\n\n\n```js\nconsole.log(\'salut\')\n\n```\n\n> test\n\n','2021-08-06 17:32:49','https://cdn.filestackcontent.com/Wx6fLFtJRNG8etHqIhES');
/*!40000 ALTER TABLE `article` ENABLE KEYS */;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `commentId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `articleId` int(11) DEFAULT NULL,
  `contenu` text NOT NULL,
  `userId` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`commentId`),
  KEY `fk_userId` (`userId`),
  KEY `fk_articleId_comment` (`articleId`),
  CONSTRAINT `fk_articleId_comment` FOREIGN KEY (`articleId`) REFERENCES `article` (`articleId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_userId_comment` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (7,5,'update',1,'2021-07-21 00:00:00'),(9,5,'test',6,'2021-08-26 00:00:00'),(10,16,'test',1,'2021-08-03 17:12:31'),(11,16,'test again',1,'2021-08-03 17:13:45'),(12,16,'re-test',1,'2021-08-03 17:13:56'),(13,11,'\"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\"\n\n',1,'2021-08-03 17:15:05'),(14,11,'ouai',1,'2021-08-03 17:15:47'),(15,11,'test',1,'2021-08-03 17:18:55'),(16,11,'re-test',1,'2021-08-03 17:20:51'),(17,11,'re-retest',1,'2021-08-03 17:21:17'),(18,11,'re-retest',1,'2021-08-03 17:21:17'),(19,10,'test',1,'2021-08-03 17:24:14'),(20,10,'test',1,'2021-08-03 17:24:53'),(21,10,'test',1,'2021-08-03 17:25:03'),(22,10,'new test',1,'2021-08-03 17:26:31'),(23,10,'ouai',1,'2021-08-03 17:28:26'),(24,16,'test another account',11,'2021-08-04 14:06:12');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `pseudo` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'KevinRSI','test2@gmail.com','$2b$11$NBbFPOEt2Q3YNJCFjqYuoeyIKnumktPUCvDLj5vlPiQgsQdlz/plG','admin','https://cdn.filestackcontent.com/CELsSdlQrqBupvukRlhg'),(6,'KRSIupdate2','testuser@gmail.com','$2b$11$qAw2ptM3wowsp03cHz37MerRAU2rUjqRNFDb2iAeYQlQ6ti5viVmW','user',NULL),(9,'testReg','testreg@test.com','$2b$11$EoIDc0cC3P4imQbbvhyqJe/CR.WBgpPJZRLr6ZJKsRXD9Huwtt366','user',NULL),(10,'testestest','retest@gmail.com','$2b$11$WNJzz1Y3HALPVtnN6ataK.erQRPh8mTj2Glmjfg9wZ.zkvEbfk/fy','user',NULL),(11,'salut','testnew@gmail.com','$2b$11$R2BfNcyxQCEncz5C4o4rCunw79x0K/YrLTKNC0M6xwmqPhz40xyda','admin',NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-09 15:18:16
