import Joi from 'joi';
export function validatorUser(value, option) {
    let schema = "";
    if (option === "add") {
        schema = Joi.object({
            pseudo: Joi.string()
                .min(1)
                .max(50)
                .required(),

            email: Joi.string()
                .email()
                .required(),

            password: Joi.string()
                .required()
        })
    } else if (option === "update") {
        schema = Joi.object({
            pseudo: Joi.string()
                .min(1)
                .max(50),

            email: Joi.string()
                .email(),

            password: Joi.string(),

            role: Joi.string(),
        })
    }

    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    const { error, data } = schema.validate(value, options);

    if (error) {
        return error;
    } else {
        return true
    }
};

export function validatorArticle(value, option) {
    let schema = "";
    if (option === "add") {
        schema = Joi.object({
            userId: Joi.number()
                .required(),

            text: Joi.string()
                .required(),

            title: Joi.string()
                .required(),

            date: Joi.date()
                .required(),

            image: Joi.string()
                .allow(null)
        })
    } else if (option === "update") {
        schema = Joi.object({
            userId: Joi.number(),

            text: Joi.string(),

            title: Joi.string(),

            date: Joi.date(),

            image: Joi.string()
                .allow(null)
        })
    }

    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    const { error, data } = schema.validate(value, options);

    if (error) {
        return error;
    } else {
        return true
    }
};


export function validatorComment(value, option) {
    let schema = "";
    if (option === "add") {
        schema = Joi.object({
            userId: Joi.number()
                .required(),

            articleId: Joi.number()
                .required(),

            contenu: Joi.string()
                .required(),

            date: Joi.date()
                .required()
        })
    } else if (option === "update") {
        schema = Joi.object({
            userId: Joi.number(),

            articleId: Joi.number(),

            contenu: Joi.string(),

            date: Joi.date()
        })
    }

    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    const { error, data } = schema.validate(value, options);

    if (error) {
        return error;
    } else {
        return true
    }
};