import { User } from "../entity/user";
import { connection } from "./connection";

export class UserRepository {
    async findByEmail(email) {
        let [rows] = await connection.execute('SELECT * FROM user WHERE email=?', [email]);
        if (rows.length === 1) {
            return new User(rows[0].pseudo, rows[0].email, rows[0].password, rows[0].role, rows[0].avatar, rows[0].id);
        }
        return null;
    }

    async lastUsers(){
        let [rows] = await connection.execute('SELECT pseudo FROM user ORDER BY id DESC LIMIT 5');
        let users = [];
        for (const row of rows) {
            let instance = row.pseudo
            users.push(instance);
        }
        return users
    }

    async findById(id){
        let [rows] = await connection.execute('SELECT * FROM user WHERE id=?', [id]);
        if (rows.length === 1) {
            return new User(rows[0].pseudo, rows[0].email, rows[0].password, rows[0].role, rows[0].avatar, rows[0].id);
        }
        return null;
    }

    async findByPseudo(pseudo) {
        let [rows] = await connection.execute('SELECT * FROM user WHERE pseudo=?', [pseudo]);
        if (rows.length === 1) {
            return new User(rows[0].pseudo, rows[0].email, rows[0].password, rows[0].role, rows[0].avatar, rows[0].id);
        }
        return null;
    }

    async addUser(item) {
        let [data] = await connection.execute('INSERT INTO user (pseudo,email,password,role, avatar) VALUES(?,?,?,?,?)', [item.pseudo, item.email, item.password, item.role, item.avatar]);
        return item.id = data.insertId
    }

    async deleteUser(id){
        let [rows] = await connection.execute('DELETE FROM user WHERE id=?', [id]);
        if (rows.affectedRows === 1) {
            return true;
        } return false;
    }

    async updateUser(item){
        let data = await connection.execute('UPDATE user SET pseudo=?, email=?, password=?, role=?, avatar=? WHERE id=?', [item.pseudo, item.email, item.password, item.role,item.avatar, item.id]);
        return data;
    }
}