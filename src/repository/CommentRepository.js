import { Comments } from "../entity/Comments";
import { connection } from "./connection";

export class CommentRepo {
    async findAllByArticle(id, limit = 10, offset = 0) {
        let [rows] = await connection.execute('SELECT comments.*, user.pseudo, user.avatar, user.role FROM comments LEFT JOIN user ON comments.userId = user.id WHERE articleId = ? ORDER BY commentId ASC LIMIT ? OFFSET ?', [id, limit, offset]);
        if (rows.length != 0) {
            let comments = [];
            for (const row of rows) {
                let instance = new Comments(row.articleId, row.contenu, row.userId, row.date, row.commentId);
                let user = {
                    pseudo: row.pseudo,
                    role: row.role,
                    avatar: row.avatar
                }
                Object.assign(instance, { user })
                comments.push(instance)
            }
            return comments
        } return null

    }

    async updateComment(item) {
        let data = await connection.execute('UPDATE comments SET contenu=? WHERE commentId=?', [item.contenu, item.commentId]);
        return data;
    }

    async addComment(item) {
        let [data] = await connection.execute('INSERT INTO comments (articleId, contenu, userId, date) VALUES (?,?,?,?)', [item.articleId, item.contenu, item.userId, item.date]);
        return item.commentId = data.insertId;
    }

    async deleteComment(id) {
        await connection.execute('DELETE FROM comments WHERE commentId=?', [id]);
        return true;
    }

    async findCommById(id) {
        let [rows] = await connection.execute('SELECT * FROM comments WHERE commentId=?', [id]);
        if (rows.length === 1) {
            return new Comments(rows[0].articleId, rows[0].contenu, rows[0].userId, rows[0].date, rows[0].commentId)
        } else {
            return null
        }
    }
}