import { Article } from "../entity/Article";
import { Comments } from "../entity/Comments";
import { connection } from "./connection";

export class ArticleRepo {
    async findAllArticle(limit = 10, offset = 0) {
        let [rows] = await connection.execute('SELECT * FROM article ORDER BY articleId DESC LIMIT ? OFFSET ? ', [limit, offset]);
        if (rows.length != 0) {
            let articles = [];
            for (const row of rows) {
                let instance = new Article(row.userId, row.title, row.text, row.date, row.image, row.articleId);
                articles.push(instance);
            }
            return articles
        } return null
    }

    async updateArticle(item) {
        let data = await connection.execute('UPDATE article SET title=?, text=?, date=?, image=? WHERE articleId=?', [item.title, item.text, item.date, item.image, item.articleId]);
        return data
    }

    async addArticle(item) {
        let [data] = await connection.execute('INSERT INTO article (userId, title, text, date, image) VALUES (?,?,?,?,?)', [item.userId, item.title, item.text, item.date, item.image]);
        return item.articleId = data.insertId;
    }

    async deleteArticle(id) {
        await connection.execute('DELETE FROM article WHERE articleId=?', [id]);
        return true
    }

    

    async findArticleById(id){
        let [rows] = await connection.execute('SELECT * FROM article WHERE articleId=?', [id]);
        if (rows.length != 0) {
        return new Article(rows[0].userId, rows[0].title, rows[0].text, rows[0].date, rows[0].image, rows[0].articleId);
        } return null
    }

    async articleByUser(id){
        let [rows] = await connection.execute('SELECT * FROM article  WHERE userId=? ORDER BY articleId DESC', [id]);
        if (rows.length != 0) {
            let articles = [];
            for (const row of rows) {
                let instance = new Article(row.userId, row.title, row.text, row.date, row.image, row.articleId);
                articles.push(instance);
            }
            return articles
        } return null
    }
}

