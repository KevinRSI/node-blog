export class Comments{
    commentId;
    articleId;
    contenu;
    userId;
    date;
    constructor(articleId, contenu, userId, date, commentId=null){
        this.articleId = articleId;
        this.contenu = contenu;
        this.userId = userId;
        this.date = date;
        this.commentId = commentId;
    }
}