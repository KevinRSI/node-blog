export class Article {
    articleId;
    userId;
    title;
    text;
    date;
    image;
    constructor(userId, title, text, date, image, articleId = null){
        this.articleId = articleId;
        this.userId = userId;
        this.title = title;
        this.text = text;
        this.date = date;
        this.image = image;
    }
}