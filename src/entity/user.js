export class User {
    id;
    pseudo;
    email;
    password;
    role;
    avatar;
    constructor(pseudo, email, password, role='user', avatar=null, id=null){
        this.pseudo = pseudo;
        this.email = email;
        this.password = password;
        this.role = role;
        this.avatar = avatar
        this.id = id;
    }
    toJSON() {
        return {
            id: this.id,
            pseudo: this.pseudo,
            email: this.email,
            role: this.role,
            avatar: this.avatar
        }
    }
}