import { Router } from "express";
import { User } from "../entity/user";
import { UserRepository } from "../repository/UserRepository";
import bcrypt from 'bcrypt';
import { generateToken } from "../utils/token";
import passport from "passport";
import { validatorUser } from "../utils/validator";

export const userController = Router();
userController.get('/find/acc', passport.authenticate('jwt', { session: false }), async (req, res) => {
    res.json(req.user)
})

userController.get('/last', async (req, res) => {
    try {
        let data = await new UserRepository().lastUsers();
        res.json(data)
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})

userController.get('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    if (req.user.role === "admin" || req.user.id === req.params.id) {
        try {
            let data = await new UserRepository().findById(req.params.id);
            res.json(data)
        } catch (error) {
            console.log(error);
            res.status(500).json(error);
        }
    } else {
        res.status(403).json({ error: "You don't have the rights to do this" });
    }
})

userController.post('/register', async (req, res) => {
    try {
        let val = validatorUser(req.body, 'add');

        if (val != true) {
            return res.status(422).json(val.details[0].message);
        } else {
            const newUser = new User();
            Object.assign(newUser, req.body);
            const exists = await new UserRepository().findByEmail(newUser.email);
            const pseudo_exists = await new UserRepository().findByPseudo(newUser.pseudo);

            if (exists || pseudo_exists) {
                res.status(400).json({ error: 'Email and/or Pseudo already taken' });
                return;
            }
            newUser.role = 'user';
            newUser.password = await bcrypt.hash(newUser.password, 11);

            await new UserRepository().addUser(newUser);
            res.status(201).json({
                user:newUser,
                token: generateToken({
                    email: newUser.email,
                    id:newUser.id,
                    role:newUser.role
                })
            });
    
        }




    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});

userController.post('/login', async (req, res) => {
    try {
        const user = await new UserRepository().findByPseudo(req.body.pseudo);
        if (user) {
            let samePWD = await bcrypt.compare(req.body.password, user.password)
            if (samePWD) {
                res.json({
                    user,
                    token: generateToken({
                        pseudo: user.pseudo,
                        email: user.email,
                        id: user.id,
                        role: user.role
                    })
                });
                return
            }
        }
        res.status(401).json({ error: 'Wrong email / password' });
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});

userController.delete('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    if (req.user.role === "admin" || req.user.id == req.params.id) {
        try {
            let data = await new UserRepository().deleteUser(req.params.id);
            if (data) {
                return res.status(204).json({ message: 'user deleted' });
            }
            return res.status(404).json({ error: "user does not exist" });
        } catch (error) {
            console.log(error);
            res.status(500).json(error);
        }
    } else if (req.user.role === 'user') {
        res.status(403).json({ error: "You don't have the rights to do this" });
    }

});

userController.patch('/:id', passport.authenticate("jwt", { session: false }), async (req, res) => {
    let role = req.user.role;
    if (role === "admin" || req.user.id === req.params.id) {
        try {
            let setRole;

            let data = await new UserRepository().findById(req.params.id);
            if (!data) {
                return res.status(404).json({ error: "user does not exist" });
            }

            role != "admin" ? setRole = "user" : setRole = data.role;

            let user = new User();
            Object.assign(user, { ...{ ...data, ...req.body }, role: setRole })
            user.password = await bcrypt.hash(user.password, 11);
            let val = validatorUser(user, 'update');

            if (val != true) {
                return res.status(422).json(val.details[0].message);
            } else {

                await new UserRepository().updateUser(user);
                res.status(202).end()
            }

        } catch (error) {
            console.log(error);
            res.status(500).json(error);
        }
    } else {
        res.status(401).json({ error: "You don't have the rights to do this" });
    }
});


