import { Router } from "express";
import passport from "passport";
import { Comments } from "../entity/Comments";
import { CommentRepo } from "../repository/CommentRepository";
import { validatorComment } from "../utils/validator";

export const commentController = Router();
const commRepo = new CommentRepo();

commentController.post('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        let comment = new Comments()
        Object.assign(comment,{...req.body, userId:req.user.id} )
        let val = validatorComment(comment, 'add');
        if (val != true) {
            return res.status(422).json(val.details[0].message);
        } else {
            await commRepo.addComment(comment);
            return res.status(201).json(comment);
        }
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})

commentController.get('/:id', async (req, res) => {
    try {
        let data = await commRepo.findAllByArticle(req.params.id, req.query.limit, req.query.offset);
        if (data) {
            return res.json(data);
        } else {
            return null
        }
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})

commentController.patch('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    let role = req.user.role;

    try {
        let data = await commRepo.findCommById(req.params.id);
        if (data && (role = 'admin' || data.userId === req.user.id) ) {

            let update = new Comments();
            Object.assign(update, { ...data, ...req.body })
            let val = validatorComment(update, 'update');
    
            if (val != true) {
                return res.status(422).json(val.details[0].message);
            } else {
    
                await commRepo.updateComment(update);
                res.status(202).end()
            }
        }
        return res.status(404).json({ error: "unable to process" });

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})

commentController.delete('/:id',passport.authenticate('jwt', { session: false }), async (req,res)=>{
    let role = req.user.role;
    try {
        let data = await commRepo.findCommById(req.params.id);
        if (data && (role==='admin' || data.userId === req.user.id)) {
            await commRepo.deleteComment(req.params.id);
            return res.status(204).json({message: "comment deleted"});
        } else {
            res.status(404).json({message: "unable to process"});
        }
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
} )
