import { Router } from "express";
import { ArticleRepo } from "../repository/ArticleRepository";
import { validatorArticle } from "../utils/validator";
import passport from "passport";
import { Article } from "../entity/Article";

export const articleController = Router();

const artRepo = new ArticleRepo();

articleController.get('/', async (req, res) => {
    try {
        let data = await artRepo.findAllArticle(req.query.limit, req.query.offset);
        if (data) {
            return res.json(data);
        } return res.status(404).end()
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});

articleController.get('/user/:id', async (req, res) => {
    try {
        let data = await artRepo.articleByUser(req.params.id);
        if (data) {
            return res.json(data);
        } return null
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});

articleController.post('/', passport.authenticate("jwt", { session: false }), async (req, res) => {
    try {
        let article = new Article();
        Object.assign(article, { ...{userId: req.user.id}, ...req.body});

        let val = validatorArticle(article, 'add');
        if (val != true) {
            return res.status(422).json(val.details[0].message);
        } else {
            let data = await artRepo.addArticle(article);
            res.status(201).json(article);
        }
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});

articleController.delete('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        let exists = await artRepo.findArticleById(req.params.id);
        if (exists && (exists.userId === req.user.id || req.user.role === "admin")) {
            await artRepo.deleteArticle(req.params.id);
            return res.status(204).json({ message: "article deleted" })
        } else {
            res.status(404).json({ message: "Unable to process" })
        }
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});

articleController.patch('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        let role = req.user.role;
        let data = await artRepo.findArticleById(req.params.id);
        if (data && (data.userId === req.user.id || role === "admin")) {
            let article = new Article();
            Object.assign(article, {...data, ...req.body})
            let val = validatorArticle(article, 'update');
            if (val != true) {
                return res.status(422).json(val.details[0].message);
            } else {
                await artRepo.updateArticle(article);
                res.status(202).json(article);
            }
        }
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});

articleController.get('/:id', async (req, res)=>{
    try {
        let data = await artRepo.findArticleById(req.params.id);
        if (data) {
            return res.json(data);
        } return null
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});