import express from 'express';
import cors from 'cors';
import { userController } from './controllers/UserController';
import { configurePassport } from './utils/token';
import passport from 'passport';
import { articleController } from './controllers/ArticleController';
import { commentController } from './controllers/CommentController';

export const server = express();

configurePassport();

server.use(express.json());
server.use(cors());
server.use(passport.initialize())
server.use('/api/user', userController);
server.use('/api/article', articleController);
server.use('/api/comments', commentController);
